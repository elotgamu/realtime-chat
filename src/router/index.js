import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Chat from "../views/Chat.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  // {
  //   path: "/about",
  //   name: "about",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue")
  // },
  {
    path: "/chat",
    name: "chat",
    component: Chat,
    props: true,
    beforeEnter: (to, from, next) => {
      const { name } = to.params;
      if (name && name !== "") {
        return next();
      }
      return next({ name: "home" });
    }
  }
];

const router = new VueRouter({
  routes
});

export default router;
